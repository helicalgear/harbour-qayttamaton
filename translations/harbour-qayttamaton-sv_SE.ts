<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="sv_SE">
<context>
    <name>AboutThisApp</name>
    <message>
        <source>About Qayttamaton</source>
        <translation>Om Qayttamation</translation>
    </message>
    <message>
        <source>Contact developer</source>
        <translation>Kontakta utvecklaren</translation>
    </message>
    <message>
        <source>Terms of Service</source>
        <translation>Användarvillkor</translation>
    </message>
    <message>
        <source>Privacy Policy</source>
        <translation>Integritetspolicy</translation>
    </message>
    <message>
        <source>About Qyttamaton</source>
        <translation>Om Qayttamation</translation>
    </message>
    <message>
        <source>Access to source code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <source>Special Thanks</source>
        <translation>Särskilt tack</translation>
    </message>
    <message>
        <source>Japan Qt Users Group</source>
        <translation>Japan Qt Users Group</translation>
    </message>
    <message>
        <source>About UNTAPPD</source>
        <translation>Om UNTAPPD</translation>
    </message>
    <message>
        <source>Unofficial UNTAPPD client for SailfishOS</source>
        <translation>Inofficiell UNTAPPED klient för SailfishOS</translation>
    </message>
    <message>
        <source>Contributers</source>
        <translation>Medverkande</translation>
    </message>
</context>
<context>
    <name>Activity</name>
    <message>
        <source>Recent Activity</source>
        <translation>Senaste aktiviteter</translation>
    </message>
    <message>
        <source>No recent activity</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>AddComments</name>
    <message>
        <source>Add Comment</source>
        <translation>Lägg till kommentar</translation>
    </message>
</context>
<context>
    <name>Authentication</name>
    <message>
        <source>Authentication</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please Login to UNTAPPD
with external browser...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please tap below button if the external browser is not opened automatically.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Tap here to login</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BadgeDetails</name>
    <message>
        <source>MM/dd/yyyy</source>
        <translation>yyyy/MM/dd</translation>
    </message>
    <message>
        <source>Created at: %1</source>
        <translation>Skapad %1</translation>
    </message>
</context>
<context>
    <name>Badges</name>
    <message>
        <source>Load More</source>
        <translation>Ladda mer</translation>
    </message>
    <message>
        <source>Badges</source>
        <translation>Märken</translation>
    </message>
</context>
<context>
    <name>BeerActivity</name>
    <message>
        <source>Activity of %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BeerCheckedIn</name>
    <message>
        <source>Comment</source>
        <translation>Kommentera</translation>
    </message>
    <message>
        <source>Toasted</source>
        <translation>Skålade</translation>
    </message>
    <message>
        <source>Toast</source>
        <translation>Skål</translation>
    </message>
    <message>
        <source>Toasted !</source>
        <translation>Glaset höjt!</translation>
    </message>
    <message>
        <source>Untoasted !</source>
        <translation>Glaset nertaget!</translation>
    </message>
    <message>
        <source>Toast/Untoast Failed...</source>
        <translation>Höj/sänk glaset misslyckades...</translation>
    </message>
</context>
<context>
    <name>BeerDetail</name>
    <message>
        <source>Check In</source>
        <translation>Checka in</translation>
    </message>
    <message>
        <source>TOTAL</source>
        <translation>TOTALT</translation>
    </message>
    <message>
        <source>UNIQUE</source>
        <translation>UNIKA</translation>
    </message>
    <message>
        <source>MONTHLY</source>
        <translation>Månatlig</translation>
    </message>
    <message>
        <source>YOU</source>
        <translation>DU</translation>
    </message>
    <message>
        <source>Global</source>
        <translation>Globalt</translation>
    </message>
    <message>
        <source>You Rate</source>
        <translation>Ditt betyg</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>Beskrivning:</translation>
    </message>
    <message>
        <source>Add to Wish List</source>
        <translation>Lägg till i önskelistan</translation>
    </message>
    <message>
        <source>Remove from Wish List</source>
        <translation>Ta bort från önskelistan</translation>
    </message>
    <message>
        <source>Brewery:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Activity of this beer</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BeerTrending</name>
    <message>
        <source>Wish List is not existed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Beer Trending</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Total Count: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Your Count: %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BeersOfBrewery</name>
    <message>
        <source>Beers of %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BreweryActivity</name>
    <message>
        <source>Activity of %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BreweryDetail</name>
    <message>
        <source>TOTAL</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>UNIQUE</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>MONTHLY</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>YOU</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Description:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Contact:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Beers of this brewery</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Activity of this brewery</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CheckIn</name>
    <message>
        <source>Check In</source>
        <translation>Checka in</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <source>Rating:</source>
        <translation>Betyg:</translation>
    </message>
    <message>
        <source>Venue:</source>
        <translation>Plats:</translation>
    </message>
</context>
<context>
    <name>CheckinDetails</name>
    <message>
        <source>Checkin Details</source>
        <translation>Checkin Detaljer</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Plats</translation>
    </message>
    <message>
        <source>Brewery</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Qayttamaton</source>
        <translation>Qayttamaton</translation>
    </message>
</context>
<context>
    <name>DeleteComments</name>
    <message>
        <source>%1 %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delete Comment</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Modify</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Modify Comment</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FriendRequest</name>
    <message>
        <source>Add Friend</source>
        <translation>Lägg till vän</translation>
    </message>
    <message>
        <source>Realy want to be a friend ?</source>
        <translation>Är du säker?</translation>
    </message>
    <message>
        <source>Remove Friend</source>
        <translation>Ta bort vän</translation>
    </message>
    <message>
        <source>Realy want to remove from friends ?</source>
        <translation>Är du säker?</translation>
    </message>
    <message>
        <source>Accept the request</source>
        <translation>Acceptera förfrågan</translation>
    </message>
    <message>
        <source>Realy want to accept the request ?</source>
        <translation>Är du säker?</translation>
    </message>
    <message>
        <source>Reject the request</source>
        <translation>Avböj förfrågan</translation>
    </message>
    <message>
        <source>Realy want to Reject the request ?</source>
        <translation>Är du säker?</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Account Info</source>
        <translation>Kontoinformation</translation>
    </message>
    <message>
        <source>Name of Beer</source>
        <translation>Ölens namn</translation>
    </message>
    <message>
        <source>Recent Activity</source>
        <translation>Senaste aktiviteter</translation>
    </message>
    <message>
        <source>About This App</source>
        <translation>Om Qayttamation</translation>
    </message>
    <message>
        <source>Near By Activity</source>
        <translation>Aktiviteter i närheten</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <source>TimeLines</source>
        <translation>Tidslinje</translation>
    </message>
    <message>
        <source>Search Beer</source>
        <translation>Sök efter öl</translation>
    </message>
    <message>
        <source>Friend Requests</source>
        <translation>Vän förfrågan</translation>
    </message>
    <message>
        <source>News Feeds</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Notifications</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Notifications for you</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Beer Trending</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>NewsList</name>
    <message>
        <source>Reload</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>News Feeds</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>NotificationsList</name>
    <message>
        <source>Notifications</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>toasted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>commented on</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 %2 %3 your check-in of %5 by %6</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No notification for you</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PendingFriends</name>
    <message>
        <source>No Pending Reqest</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <source>TOTAL</source>
        <translation>TOTALT</translation>
    </message>
    <message>
        <source>UNIQUE</source>
        <translation>UNIKA</translation>
    </message>
    <message>
        <source>FRIENDS</source>
        <translation>VÄNNER</translation>
    </message>
    <message>
        <source>BADGES</source>
        <translation>MÄRKEN</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Logga ut</translation>
    </message>
    <message>
        <source>Remove from Firends</source>
        <translation>Ta bort vän</translation>
    </message>
    <message>
        <source>Add to Friends</source>
        <translation>Lägg till vän</translation>
    </message>
    <message>
        <source>Accept the Friend Request</source>
        <translation>Acceptera vänförfrågan</translation>
    </message>
    <message>
        <source>Reject the Friend Request</source>
        <translation>Avböj vänförfrågan</translation>
    </message>
    <message>
        <source>Not yet approved your request...</source>
        <translation>Har inte svarat på din förfrågan än...</translation>
    </message>
    <message>
        <source>%1's Wish List
Updated on %2</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SearchBeerResult</name>
    <message>
        <source>No beer found</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ThepubLocal</name>
    <message>
        <source>Reload</source>
        <translation>Ladda om</translation>
    </message>
    <message>
        <source>Load More</source>
        <translation>Ladda mer</translation>
    </message>
    <message>
        <source>NearBy Activity</source>
        <translation>Händelser i närheten</translation>
    </message>
</context>
<context>
    <name>UserCheckedIn</name>
    <message>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
</context>
<context>
    <name>UserWishList</name>
    <message>
        <source>%1&apos;s Wish List</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Added On %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Wish List is not existed</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>VenueActivity</name>
    <message>
        <source>Activity of %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>VenueCheckedIn</name>
    <message>
        <source>category: %1</source>
        <translation>Kategori %1</translation>
    </message>
</context>
<context>
    <name>VenueDetail</name>
    <message>
        <source>Activity of this venue</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>TOTAL</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>MONTHLY</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>YOU</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Contact:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1, %2, %3, %4</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>USER</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>View on GoogleMap</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>View on FourSquare</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>VenuesSearch</name>
    <message>
        <source>Name of Venues</source>
        <translation>Namn på plats</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>