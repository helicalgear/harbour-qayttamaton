<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="ja_JP">
<context>
    <name>AboutThisApp</name>
    <message>
        <source>About Qayttamaton</source>
        <translation>Qayttamatonについて</translation>
    </message>
    <message>
        <source>Contact developer</source>
        <translation>開発者に連絡</translation>
    </message>
    <message>
        <source>Terms of Service</source>
        <translation>利用規約</translation>
    </message>
    <message>
        <source>Privacy Policy</source>
        <translation>プライバシーポリシー</translation>
    </message>
    <message>
        <source>About Qyttamaton</source>
        <translation>Qayttamatonについて</translation>
    </message>
    <message>
        <source>Access to source code</source>
        <translation>ソースコードを</translation>
    </message>
    <message>
        <source>Special Thanks</source>
        <translation>謝辞</translation>
    </message>
    <message>
        <source>Japan Qt Users Group</source>
        <translation>日本Qtユーザー会</translation>
    </message>
    <message>
        <source>About UNTAPPD</source>
        <translation>UNTAPPDについて</translation>
    </message>
    <message>
        <source>Unofficial UNTAPPD client for SailfishOS</source>
        <translation>SailfishOS用非公式UNTAPPDクライアント</translation>
    </message>
    <message>
        <source>Contributers</source>
        <translation>貢献者</translation>
    </message>
</context>
<context>
    <name>Activity</name>
    <message>
        <source>Recent Activity</source>
        <translation>直近のアクティビティ</translation>
    </message>
    <message>
        <source>No recent activity</source>
        <translation>直近のアクティビティはありません。</translation>
    </message>
</context>
<context>
    <name>AddComments</name>
    <message>
        <source>Add Comment</source>
        <translation>コメントを追加</translation>
    </message>
</context>
<context>
    <name>Authentication</name>
    <message>
        <source>Authentication</source>
        <translation>ユーザー認証</translation>
    </message>
    <message>
        <source>Please Login to UNTAPPD
with external browser...</source>
        <translation>既定のブラウザでUNTAPPDに
サインインしてください。</translation>
    </message>
    <message>
        <source>Please tap below button if the external browser is not opened automatically.</source>
        <translation>外部ブラウザが自動的に表示されない場合には下のボタンをタップして下さい。</translation>
    </message>
    <message>
        <source>Tap here to login</source>
        <translation>ログイン</translation>
    </message>
</context>
<context>
    <name>BadgeDetails</name>
    <message>
        <source>MM/dd/yyyy</source>
        <translation>yyyy年MM月dd日</translation>
    </message>
    <message>
        <source>Created at: %1</source>
        <translation>%1に投稿</translation>
    </message>
</context>
<context>
    <name>Badges</name>
    <message>
        <source>Load More</source>
        <translation>追加読込み</translation>
    </message>
    <message>
        <source>Badges</source>
        <translation>バッジ</translation>
    </message>
</context>
<context>
    <name>BeerActivity</name>
    <message>
        <source>Activity of %1</source>
        <translation>%1のアクティビティ</translation>
    </message>
</context>
<context>
    <name>BeerCheckedIn</name>
    <message>
        <source>Comment</source>
        <translation>コメント</translation>
    </message>
    <message>
        <source>Toasted</source>
        <translation>乾杯！</translation>
    </message>
    <message>
        <source>Toast</source>
        <translation>乾杯する</translation>
    </message>
    <message>
        <source>Toasted !</source>
        <translation>乾杯しました！</translation>
    </message>
    <message>
        <source>Untoasted !</source>
        <translation>乾杯を取り消しました！</translation>
    </message>
    <message>
        <source>Toast/Untoast Failed...</source>
        <translation>乾杯／取り消しできません...</translation>
    </message>
</context>
<context>
    <name>BeerDetail</name>
    <message>
        <source>Check In</source>
        <translation>チェックイン</translation>
    </message>
    <message>
        <source>TOTAL</source>
        <translation>トータル</translation>
    </message>
    <message>
        <source>UNIQUE</source>
        <translation>ユニーク</translation>
    </message>
    <message>
        <source>MONTHLY</source>
        <translation>月間</translation>
    </message>
    <message>
        <source>YOU</source>
        <translation>あなた</translation>
    </message>
    <message>
        <source>Global</source>
        <translation>グローバル</translation>
    </message>
    <message>
        <source>You Rate</source>
        <translation>あなたの評価</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>解説:</translation>
    </message>
    <message>
        <source>Add to Wish List</source>
        <translation>WishListに追加</translation>
    </message>
    <message>
        <source>Remove from Wish List</source>
        <translation>WishListから削除</translation>
    </message>
    <message>
        <source>Brewery:</source>
        <translation>醸造所</translation>
    </message>
    <message>
        <source>Activity of this beer</source>
        <translation>このビールのアクティビティ</translation>
    </message>
</context>
<context>
    <name>BeerTrending</name>
    <message>
        <source>Wish List is not existed</source>
        <translation>Wish Listがありません</translation>
    </message>
    <message>
        <source>Beer Trending</source>
        <translation>ビールのトレンド</translation>
    </message>
    <message>
        <source>Total Count: %1</source>
        <translation>飲まれた回数: %1</translation>
    </message>
    <message>
        <source>Your Count: %1</source>
        <translation>飲んだ回数: %1</translation>
    </message>
</context>
<context>
    <name>BeersOfBrewery</name>
    <message>
        <source>Beers of %1</source>
        <translation>%1のビール</translation>
    </message>
</context>
<context>
    <name>BreweryActivity</name>
    <message>
        <source>Activity of %1</source>
        <translation>%1のアクティビティ</translation>
    </message>
</context>
<context>
    <name>BreweryDetail</name>
    <message>
        <source>TOTAL</source>
        <translation>トータル</translation>
    </message>
    <message>
        <source>UNIQUE</source>
        <translation>ユニーク</translation>
    </message>
    <message>
        <source>MONTHLY</source>
        <translation>月間</translation>
    </message>
    <message>
        <source>YOU</source>
        <translation>あなた</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>解説: </translation>
    </message>
    <message>
        <source>Contact:</source>
        <translation>連絡先: </translation>
    </message>
    <message>
        <source>Beers of this brewery</source>
        <translation>この醸造所のビール</translation>
    </message>
    <message>
        <source>Activity of this brewery</source>
        <translation>この醸造所のアクティビティ</translation>
    </message>
</context>
<context>
    <name>CheckIn</name>
    <message>
        <source>Check In</source>
        <translation>チェックイン</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation>コメント:</translation>
    </message>
    <message>
        <source>Rating:</source>
        <translation>評価:</translation>
    </message>
    <message>
        <source>Venue:</source>
        <translation>飲んだ場所:</translation>
    </message>
</context>
<context>
    <name>CheckinDetails</name>
    <message>
        <source>Checkin Details</source>
        <translation>チェックインの詳細</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>ロケーション</translation>
    </message>
    <message>
        <source>Brewery</source>
        <translation>醸造所</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Qayttamaton</source>
        <translation>Qayttamaton</translation>
    </message>
</context>
<context>
    <name>DeleteComments</name>
    <message>
        <source>%1 %2</source>
        <translation>%2 %1</translation>
    </message>
    <message>
        <source>Delete Comment</source>
        <translation>コメントを削除</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <source>Modify</source>
        <translation>編集</translation>
    </message>
    <message>
        <source>Modify Comment</source>
        <translation>コメントを編集</translation>
    </message>
</context>
<context>
    <name>FriendRequest</name>
    <message>
        <source>Add Friend</source>
        <translation>友達に追加</translation>
    </message>
    <message>
        <source>Realy want to be a friend ?</source>
        <translation>友達申請しますか？</translation>
    </message>
    <message>
        <source>Remove Friend</source>
        <translation>友達を削除</translation>
    </message>
    <message>
        <source>Realy want to remove from friends ?</source>
        <translation>友達から削除しますか？</translation>
    </message>
    <message>
        <source>Accept the request</source>
        <translation>申請を受理</translation>
    </message>
    <message>
        <source>Realy want to accept the request ?</source>
        <translation>申請を受理しますか？</translation>
    </message>
    <message>
        <source>Reject the request</source>
        <translation>申請を却下</translation>
    </message>
    <message>
        <source>Realy want to Reject the request ?</source>
        <translation>申請を却下しますか？</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Account Info</source>
        <translation>アカウント情報</translation>
    </message>
    <message>
        <source>Name of Beer</source>
        <translation>ビール名</translation>
    </message>
    <message>
        <source>Recent Activity</source>
        <translation>最近のアクティビティ</translation>
    </message>
    <message>
        <source>About This App</source>
        <translation>Qayttamatonについて</translation>
    </message>
    <message>
        <source>Near By Activity</source>
        <translation>周囲でのチェックイン</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>検索</translation>
    </message>
    <message>
        <source>TimeLines</source>
        <translation>タイムライン</translation>
    </message>
    <message>
        <source>Search Beer</source>
        <translation>ビールを検索</translation>
    </message>
    <message>
        <source>Friend Requests</source>
        <translation>友達申請</translation>
    </message>
    <message>
        <source>News Feeds</source>
        <translation>ニュース</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>通知</translation>
    </message>
    <message>
        <source>Notifications for you</source>
        <translation>あなたへの通知</translation>
    </message>
    <message>
        <source>Beer Trending</source>
        <translation>ビールのトレンド</translation>
    </message>
</context>
<context>
    <name>NewsList</name>
    <message>
        <source>Reload</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>News Feeds</source>
        <translation>ニュース</translation>
    </message>
</context>
<context>
    <name>NotificationsList</name>
    <message>
        <source>Notifications</source>
        <translation>通知</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>toasted</source>
        <translation>乾杯</translation>
    </message>
    <message>
        <source>commented on</source>
        <translation>コメント</translation>
    </message>
    <message>
        <source>%1 %2 %3 your check-in of %5 by %6</source>
        <translation>%2 %1 が、あなたの%5 (%6) のチェックインに %3 しました。</translation>
    </message>
    <message>
        <source>No notification for you</source>
        <translation>あなた宛の通知はありません。</translation>
    </message>
</context>
<context>
    <name>PendingFriends</name>
    <message>
        <source>No Pending Reqest</source>
        <translation>友達リクエストはありません</translation>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <source>TOTAL</source>
        <translation>飲んだ数</translation>
    </message>
    <message>
        <source>UNIQUE</source>
        <translation>ユニーク</translation>
    </message>
    <message>
        <source>FRIENDS</source>
        <translation>友達</translation>
    </message>
    <message>
        <source>BADGES</source>
        <translation>バッジ</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>ログアウト</translation>
    </message>
    <message>
        <source>Remove from Firends</source>
        <translation>友達から削除する</translation>
    </message>
    <message>
        <source>Add to Friends</source>
        <translation>友達に追加する</translation>
    </message>
    <message>
        <source>Accept the Friend Request</source>
        <translation>友達申請を受理する</translation>
    </message>
    <message>
        <source>Reject the Friend Request</source>
        <translation>友達申請を却下する</translation>
    </message>
    <message>
        <source>Not yet approved your request...</source>
        <translation>友達申請は未受理です…</translation>
    </message>
    <message>
        <source>%1's Wish List
Updated on %2</source>
        <translation>%1の Wish List
%2 に更新</translation>
    </message>
</context>
<context>
    <name>SearchBeerResult</name>
    <message>
        <source>No beer found</source>
        <translation>ビールが見つかりませんでした</translation>
    </message>
</context>
<context>
    <name>ThepubLocal</name>
    <message>
        <source>Reload</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Load More</source>
        <translation>追加読込み</translation>
    </message>
    <message>
        <source>NearBy Activity</source>
        <translation>周辺のアクティビティ</translation>
    </message>
</context>
<context>
    <name>UserCheckedIn</name>
    <message>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
</context>
<context>
    <name>UserWishList</name>
    <message>
        <source>%1&apos;s Wish List</source>
        <translation>%1の Wish List</translation>
    </message>
    <message>
        <source>Added On %1</source>
        <translation>%1 に追加</translation>
    </message>
    <message>
        <source>Wish List is not existed</source>
        <translation>Wish Listがありません</translation>
    </message>
</context>
<context>
    <name>VenueActivity</name>
    <message>
        <source>Activity of %1</source>
        <translation>%1のアクティビティ</translation>
    </message>
</context>
<context>
    <name>VenueCheckedIn</name>
    <message>
        <source>category: %1</source>
        <translation>カテゴリ: %1</translation>
    </message>
</context>
<context>
    <name>VenueDetail</name>
    <message>
        <source>Activity of this venue</source>
        <translation>このお店のアクティビティ</translation>
    </message>
    <message>
        <source>TOTAL</source>
        <translation>トータル</translation>
    </message>
    <message>
        <source>MONTHLY</source>
        <translation>月間</translation>
    </message>
    <message>
        <source>YOU</source>
        <translation>あなた</translation>
    </message>
    <message>
        <source>Contact:</source>
        <translation>連絡先: </translation>
    </message>
    <message>
        <source>%1, %2, %3, %4</source>
        <translation>%4, %3, %2, %1</translation>
    </message>
    <message>
        <source>USER</source>
        <translation>利用者数</translation>
    </message>
    <message>
        <source>View on GoogleMap</source>
        <translation>Googleマップで見る</translation>
    </message>
    <message>
        <source>View on FourSquare</source>
        <translation>FourSquareで見る</translation>
    </message>
</context>
<context>
    <name>VenuesSearch</name>
    <message>
        <source>Name of Venues</source>
        <translation>飲んだ場所の名称</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>検索</translation>
    </message>
</context>
</TS>