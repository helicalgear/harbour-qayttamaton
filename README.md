This is the unofficial UNTAPPED client for SailfishOS.

Your own account for UNTAPPED is needed to use this app.
You can get your account from the url below:

    https://untappd.com/


You need to registrate your App infomation for UNTAPPD and FOURSQUARE if you want to build this app your own environment.


1. Access the url below to registrate you app and to get the client_id & client_secret for UNTAPPD

    https://untappd.com/api/dashboard

    *You must set the "Redirect Url" to "http://localhost:8080" when you registrate your app information to use this source code.


2. Access the url below to registrate you app and to get the client_id & client_secret for FOURSQUARE

    https://developer.foursquare.com/


3. Copy (or rename) the "src/qayttamaton.h.skel" to "src/qayttamaton.h" and modify the each parameters.


I hope you enjoy to use it !

helicalgear
