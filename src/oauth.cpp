/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "oauth.h"
#include <QtNetwork>
#include <QDebug>
#include <QtGui/QDesktopServices>

const char* const authorize_url = "https://untappd.com/oauth/authenticate/?client_id=%1&response_type=code&redirect_url=%2";

OAuth::OAuth(QObject *parent) : QObject(parent)
{
}

QString OAuth::oauthClientId() const
{
    return m_clientId;
}

void OAuth::setOauthClientId(const QString &oauthClientId)
{
    m_clientId = oauthClientId;
}

QString OAuth::oauthRedirectUrl() const
{
    return m_redirectUrl;
}

void OAuth::setOauthRedirectUrl(const QString &oauthRedirectUrl)
{
    m_redirectUrl = oauthRedirectUrl;
}

QString OAuth::code() const
{
    return m_code;
}

void OAuth::getCode()
{
    auto server = new QTcpServer();
    connect(server,
            SIGNAL(newConnection()),
            this,
            SLOT(on_TcpServer_NewConnection()));
    server->listen(QHostAddress(QHostAddress::LocalHost),
                   8080);

    /**
    QUrl url(u8"https://untappd.com/oauth/authenticate/");
    QUrlQuery query;
    query.addQueryItem(u8"client_id",
                       m_clientId);
    query.addQueryItem(u8"redirect_uri",
                       m_redirectUrl);
    query.addQueryItem(u8"response_type",
                       u8"code");
    url.setQuery(query);
    auto open_process = new QProcess();

    connect(open_process,
            SIGNAL(finished(int)),
            open_process,
            SLOT(deleteLater()));
    QStringList options;
    options << url.toString();
    open_process->start(u8"xdg-open",
                        options);
    **/

    QDesktopServices::openUrl(QUrl(QString::fromUtf8(authorize_url).arg(m_clientId).arg(m_redirectUrl)));

}

void OAuth::on_TcpServer_NewConnection()
{
    auto server = reinterpret_cast<QTcpServer*>(sender());
    while (auto socket = server->nextPendingConnection())
    {
        connect(socket,
                SIGNAL(readyRead()),
                this,
                SLOT(on_TcpSocket_readyRead()));
        connect(socket,
                SIGNAL(disconnected()),
                socket,
                SLOT(deleteLater()));
    }
}

void OAuth::on_TcpSocket_readyRead()
{
    auto socket = reinterpret_cast<QTcpSocket*>(sender());
    auto response = socket->readAll();
    auto response_header = QString(response.left(response.indexOf("\r\n\r\n")));
    auto response_header_list = response_header.split(QString(u8"\r\n"));

    QRegExp reg(u8"^GET /\\?(.+) HTTP/1\\.1");
    reg.exactMatch(response_header_list[0]);
    if (reg.captureCount() != 1)
    {
        return;
    }

    QMap<QString, QString> params;
    auto match_list = reg.capturedTexts()[1].split(u8"&");
    for (const auto& param: match_list)
    {
        auto eq_idx = param.indexOf(u8"=");
        if (eq_idx > 0)
        {
            params.insert(param.left(eq_idx),
                          param.mid(eq_idx + 1));
        }
    }

    if (params[u8"error"].isEmpty() == false)
    {
        QString data(
                    u8"HTTP/1.1 200 OK\r\n" \
                    "Content-Type: text/html\r\n"
                    "Server: harbour-qayttamaton\r\n"
                    "\r\n"
                    "<HTML><HEAD><TITLE>Error Occurred</TITLE></HEAD>"
                    "<BODY><CENTER><H1>"
                    "<BR><BR>"
                    "Thank you for using Qayttamaton!!"
                    "<BR><BR>"
                    "Please close this web-page and try authentication again."
                    "</H1></CENTER></BODY>"
                    "</HTML>\r\n");
        socket->write(data.toUtf8());
        qDebug() << u8"error";
    }
    else if (params[u8"code"].isEmpty())
    {
        qDebug() << u8"empty";
    }
    else
    {
        m_code = params[u8"code"];
        QString data(
                    u8"HTTP/1.1 200 OK\r\n" \
                    "Content-Type: text/html\r\n"
                    "Server: harbour-qayttamaton\r\n"
                    "\r\n"
                    "<HTML><HEAD><TITLE>User Specified</TITLE></HEAD>"
                    "<BODY><CENTER><H1>"
                    "<BR><BR>"
                    "Thank you for using Qayttamaton!!"
                    "<BR><BR>"
                    "Please close this web-page and back to Qayttamaton app."
                    "</H1></CENTER></BODY>"
                    "</HTML>\r\n");
        socket->write(data.toUtf8());
        emit codeChanged(m_code);
    }

    socket->disconnectFromHost();
}
