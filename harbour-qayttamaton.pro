# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-qayttamaton

CONFIG += sailfishapp

QT += positioning

SOURCES += src/qayttamaton.cpp \
    src/oauth.cpp

OTHER_FILES += qml/harbour-qayttamaton.qml \
    qml/cover/CoverPage.qml \
    rpm/harbour-qayttamaton.changes.in \
    rpm/harbour-qayttamaton.spec \
    rpm/harbour-qayttamaton.yaml \
    translations/*.ts \
    harbour-qayttamaton.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

TRANSLATIONS += \
    translations/harbour-qayttamaton-sv_SE.ts \
    translations/harbour-qayttamaton-ja_JP.ts

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

#DEFINES += \
#    ID=\\\"$$_ID\\\" \
#    SECRET=\\\"$$_SECRET\\\" \
#    URL=\\\"$$_URL\\\" \
#    FS_ID=\\\"$$_FS_ID\\\" \
#    FS_SECRET=\\\"$$_FS_SECRET\\\" \
#    FS_VERSION=\\\"$$_FS_VERSION\\\"

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
#TRANSLATIONS += translations/qayttamaton-de.ts

DISTFILES += \
    qml/components/NameLabel.qml \
    qml/components/ScrollingLabel.qml \
    qml/components/Toasts.qml \
    qml/models/AbstractItemListModel.qml \
    qml/models/BeerActivityModel.qml \
    qml/models/BeerTrendingModel.qml \
    qml/models/BreweryActivityModel.qml \
    qml/models/BreweryInfo.qml \
    qml/models/NotificationsModel.qml \
    qml/models/UserWishListModel.qml \
    qml/models/VenueActivityModel.qml \
    qml/models/VenueInfo.qml \
    qml/pages/BeerActivity.qml \
    qml/pages/BeerTrending.qml \
    qml/pages/BeersOfBrewery.qml \
    qml/pages/BreweryActivity.qml \
    qml/pages/BreweryDetail.qml \
    qml/pages/DeleteComments.qml \
    qml/pages/MainPage.qml \
    qml/pages/NewsList.qml \
    qml/pages/NotificationsList.qml \
    qml/pages/SettingsStorage.qml \
    qml/pages/Authentication.qml \
    qml/pages/Profile.qml \
    qml/pages/CheckIn.qml \
    qml/pages/UserWishList.qml \
    qml/pages/VenueActivity.qml \
    qml/pages/VenueDetail.qml \
    qml/utils/UntappdAPI.js \
    qml/models/BeerInfo.qml \
    qml/pages/BeerDetail.qml \
    qml/models/ThepubLocalModel.qml \
    qml/models/UserInfo.qml \
    qml/models/AbstractSearchModel.qml \
    qml/models/AbstractCheckinsModel.qml \
    qml/models/AbstractInfoModel.qml \
    qml/pages/Activity.qml \
    qml/models/CheckinRecentModel.qml \
    qml/pages/ThepubLocal.qml \
    qml/utils/FoursquareAPI.js \
    qml/models/VenuesListModel.qml \
    qml/models/SearchBeerModel.qml \
    qml/pages/VenuesSearch.qml \
    qml/models/VenuesSearchModel.qml \
    qml/models/UserCheckinsModel.qml \
    qml/delegates/CheckinDelegate.qml \
    README.md \
    qml/components/FavStar.qml \
    qml/components/Avatar.qml \
    qml/pages/AboutThisApp.qml \
    qml/pages/SearchBeerResult.qml \
    qml/components/PageLink.qml \
    qml/models/CheckinView.qml \
    qml/pages/CheckinDetails.qml \
    qml/components/BeerCheckedIn.qml \
    qml/components/VenueCheckedIn.qml \
    qml/components/UserCheckedIn.qml \
    qml/components/BadgesCheckedIn.qml \
    qml/components/Comments.qml \
    qml/models/UserFriendsModel.qml \
    qml/pages/Friends.qml \
    qml/models/AbstractItemModel.qml \
    qml/models/PendingFriendsModel.qml \
    qml/pages/PendingFriends.qml \
    qml/pages/FriendRequest.qml \
    qml/pages/Badges.qml \
    qml/models/UserBadgesModel.qml \
    qml/pages/BadgeDetails.qml

HEADERS += \
    src/qayttamaton.h \
    src/oauth.h
