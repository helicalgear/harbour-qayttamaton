/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Foursquare API Documentation:
// https://developer.foursquare.com/docs/

// Venues Search
function getVenuesSearch(callback, param) {
    var parameter_list = [];
    for (var key in param) {
        if (!(param[key] === "" || param[key] === 0 || param[key] === "off")) {
            parameter_list.push("%1=%2".arg(key).arg(param[key]));
        }
    }
    var parameters = parameter_list.join('&');
    accessAPI(function(result, response, notifications) {
        if (result) {
            callback( Object.keys(response.venues), response.venues );
        } else {
            callback( -1, response );
        }
    }, "GET", "venues/search", "%1".arg(parameters));
}

// Access to API
function accessAPI(callback, method, endpoint, parameters) {
    var xhr = new XMLHttpRequest;
    var param_get = (method === "GET" && parameters !== "") ? "%1&".arg(parameters) : "";
    var param_post = (method === "POST") ? parameters : "";
    var url = "https://api.foursquare.com/v2/%1?%2client_id=%3&client_secret=%4&v=%5".arg(endpoint).arg(param_get).arg(fsClientId).arg(fsClientSecret).arg(fsVersion);
    xhr.open(method, url, true);
    if (method === "POST") { xhr.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' ); }
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            var jsn = JSON.parse(xhr.responseText);
            switch (xhr.status) {
            case 200:
                if ( typeof jsn.response.error === 'undefined' ) {
                    callback(true, jsn.response, {});
                } else {
                    callback(false, { "code": "000", "error_detail": jsn.response.error }, {});
                }
                break;
            default:
                callback(false, jsn.meta, {});
                break;
            }
        }
    }
    xhr.send(param_post);
}
