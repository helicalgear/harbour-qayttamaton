/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// UNTAPPD API Documentation:
// https://untappd.com/api/docs

// _Feeds_
// Activity Feed
function getCheckinRecent(callback) {
    accessAPI( function(result, response, notifications) { callback( response.checkins.count, response.checkins.items ); }, "GET", "checkin/recent", "");
}

// User Activity Feed
function getUserCheckins(callback, user_name) {
    accessAPI( function(result, response, notifications) { callback( response.checkins.count, response.checkins.items ); }, "GET", "user/checkins/%1".arg(user_name), "" );
}

// The Pub
function getThepubLocal(callback, param) {
    var parameters = "lat=%1&lng=%2&".arg(param.lat).arg(param.lng);
    parameters += "radius=%1&dist_pref=%2".arg(param.radius).arg(param.dist_pref);
    accessAPI(function(result, response, notifications) { callback( response.checkins.count, response.checkins.items ); }, "GET", "thepub/local", "%1".arg(parameters));
}

// Venue Activity Feed
function getVenueCheckins(callback, venue_id) {
    accessAPI(function(result, response, notifications) { callback( response.checkins.count, response.checkins.items ); }, "GET", "venue/checkins/%1".arg(venue_id), "");
}

// Beer Activity Feed
function getBeerCheckins(callback, bid) {
    accessAPI(function(result, response, notifications) { callback( response.checkins.count, response.checkins.items ); }, "GET", "beer/checkins/%1".arg(bid), "");
}

// Brewery Activity Feed
function getBreweryCheckins(callback, brewery_id) {
    accessAPI(function(result, response, notifications) { callback( response.checkins.count, response.checkins.items ); }, "GET", "brewery/checkins/%1".arg(brewery_id), "");
}

//Notifications
function getNotifications(callback, sw) {
    accessAPI(function(result, response, notifications) { callback( response[sw].count, response[sw].items ); }, "GET", "notifications", "");
}

// _Info_
// Checkin Details
function getCheckinView(callback, checkin_id) {
    accessAPI(function(result, response, notifications) {
        if ( result ) {
            callback(response.checkin);
        } else {
            console.log("%1: %2".arg(response.code).arg(response.error_detail))
        }
    }, "GET", "checkin/view/%1".arg(checkin_id), "");
}

// User Info
function getUserInfo(callback, user_name) {
    accessAPI(function(result, response, notifications) { callback(response.user); }, "GET", "user/info/%1".arg(user_name), "");
}

// User With List
function getUserWishlist(callback, user_name) {
    accessAPI(function(result, response, notifications) { callback(response.beers.count, response.beers.items); }, "GET", "user/wishlist/%1".arg(user_name), "");
}

// User Friends
function getUserFriends(callback, user_name) {
    accessAPI(function(result, response, notifications) { callback(response.count, response.items); }, "GET", "user/friends/%1".arg(user_name), "");
}

// User Badges
function getUserBadges(callback, user_name) {
    accessAPI(function(result, response, notifications) { callback(response.count, response.items);  }, "GET", "user/badges/%1".arg(user_name), "");
}

// User Beers
function getUserBeers(callback, user_name) {
    accessAPI(function(result, response, notifications) { callback(response.beer.count, response.beer.items); }, "GET", "user/beers/%1".arg(user_name), "");
}

// Brewery Info
function getBreweryInfo(callback, brewery_id) {
    accessAPI(function(result, response, notifications) { callback(response.brewery); }, "GET", "brewery/info/%1".arg(brewery_id), "");
}

// Beer Info
function getBeerInfo(callback, bid) {
    accessAPI(function(result, response, notifications) { callback(response.beer); }, "GET", "beer/info/%1".arg(bid), "");
}

// Venue Info
function getVenueInfo(callback, venue_id) {
    accessAPI(function(result, response, notifications) { callback(response.venue); }, "GET", "brewery/info/%1".arg(venue_id), "");
}

// Beer Trending
function getVenueInfo(callback) {
    accessAPI(function(result, response, notifications) { callback(response.count, response.items); }, "GET", "beer/trending", "");
}

// _Search_
// Search Beer
function getSearchBeer(callback, query) {
    accessAPI(function(result, response, notifications) { callback(response.beers.count, response.beers.items); }, "GET", "search/beer", "q=%1".arg(query));
}

// Search Brewery
function getSearchBrewery(callback, query) {
    accessAPI(function(result, response, notifications) { callback(response.found, response.brewery); }, "GET", "search/brewery", "q=%1".arg(query));
}

// _Actions_
// Checkin
function postCheckinAdd(callback, param) {
    var parameter_list = [];
    for (var key in param) {
        if (!(param[key] === "" || param[key] === 0 || param[key] === "off")) {
            parameter_list.push("%1=%2".arg(key).arg(param[key]));
        }
    }
    var parameters = parameter_list.join('&');
    accessAPI(function(result, response, notifications) {
        if ( result ) {
            callback( 0, response );
        } else {
            callback( -1, response );
        }
    }, "POST", "checkin/add", parameters);
}

// Toast / Untoast
function postCheckinToast(callback, checkin_id) {
    accessAPI(function(result, response, notifications) {
        if (result) {
            callback(response.result, response.like_type);
        } else {
            console.log("%1: %2".arg(response.code).arg(response.error_detail))
        }
    }, "POST", "checkin/toast/%1".arg(checkin_id), "");
}

// Pending Friends
function getUserPending(callback) {
    accessAPI(function(result, response, notifications) { callback(response.count, response.items); }, "GET", "user/pending", "");
}

// Request(Add) Friend
function getFriendRequest(callback, target_id) {
    accessAPI(function(result, response, notifications) {
        if (result) {
            callback(response.result);
        } else {
            console.log("%1: %2".arg(response.code).arg(response.error_detail))
        }
    }, "GET", "friend/request/%1".arg(target_id), "");
}

// Remove Friend
function getFriendRemove(callback, target_id) {
    accessAPI(function(result, response, notifications) {
        if (result) {
            callback(response.result);
        } else {
            console.log("%1: %2".arg(response.code).arg(response.error_detail))
        }
    }, "GET", "friend/remove/%1".arg(target_id), "");
}

// Accept Friend
function getFriendAccept(callback, target_id) {
    accessAPI(function(result, response, notifications) {
        if (result) {
            callback(response.result);
        } else {
            console.log("%1: %2".arg(response.code).arg(response.error_detail))
        }
    }, "GET", "friend/accept/%1".arg(target_id), "");
}

// Reject Friend
function getFriendReject(callback, target_id) {
    accessAPI(function(result, response, notifications) {
        if (result) {
            callback(response.result);
        } else {
            console.log("%1: %2".arg(response.code).arg(response.error_detail))
        }
    }, "GET", "friend/reject/%1".arg(target_id), "");
}

// Add Comment
function postCheckinAddcomment(callback, checkin_id, comment) {
    accessAPI(function(result, response, notifications) {
        if (result) {
            callback(response.result);
        } else {
            console.log("%1: %2".arg(response.code).arg(response.error_detail))
        }
    }, "POST", "checkin/addcomment/%1".arg(checkin_id), "comment=%1".arg(comment).replace(/%20/, '+'));
}

// Remove Comment
function postCheckinDeletecomment(callback, comment_id) {
    accessAPI(function(result, response, notifications) {
        if (result) {
            callback(response.result);
        } else {
            console.log("%1: %2".arg(response.code).arg(response.error_detail))
        }
    }, "POST", "checkin/deletecomment/%1".arg(comment_id), "");
}

// Add to Wish List
function getUserWishlistAdd(callback, bid) {
    accessAPI(function(result, response, notifications) { callback(response.result); }, "GET", "user/wishlist/add", "bid=%1".arg(bid));
}

// Remove from Wish List
function getUserWishlistDelete(callback, bid) {
    accessAPI(function(result, response, notifications) { callback(response.result); }, "GET", "user/wishlist/delete", "bid=%1".arg(bid));
}

// Foursqaure Lookup
function getVenueFoursquare_lookup(callback, venue_id) {
    accessAPI(function(result, response, notifications) { callback(response.count, response.items); }, "GET", "venue/foursquare_lookup/%1".arg(venue_id), "");
}

// Access to API
function accessAPI(callback, method, endpoint, parameters) {
    var xhr = new XMLHttpRequest;
    var param_get = (method === "GET" && parameters !== "" ) ? "%1&".arg(parameters) : "";
    var param_post = (method === "POST") ? parameters : "";
    var url = "https://api.untappd.com/v4/%1?%2access_token=%3".arg(endpoint).arg(param_get).arg(settings.readData('AccessToken', ''));
    xhr.open(method, url, true);
    if (method === "POST") { xhr.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' ); }
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            var jsn = JSON.parse(xhr.responseText);
            switch (xhr.status) {
            case 200:
                if ( typeof jsn.response.error === 'undefined' ) {
                    callback(true, jsn.response, jsn.notifications.unread_count);
                } else {
                    callback(false, { "code": jsn.meta["code"], "error_detail": jsn.meta["error_detail"] }, {});
                }
                break;
            default:
                callback(false, jsn.meta, {});
                break;
            }
        }
    }
    xhr.send(param_post);
}

// OAuth
function getAccessToken(code) {
    var xhr = new XMLHttpRequest;
    var url = "https://untappd.com/oauth/authorize/?client_id=%1&client_secret=%2&response_type=code&redirect_url=%3&code=%4".arg(clientId).arg(clientSecret).arg(redirectUrl).arg(code);
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
            case 200:
                var jsn = JSON.parse(xhr.responseText);
                settings.saveData('AccessToken', jsn.response.access_token);
                accessAPI(function(result, user_info, notification) {
                    if (result) {
                        settings.saveData("foursquare", user_info.contact.foursquare);
                        settings.saveData("twitter", user_info.contact.twitter);
                        settings.saveData("facebook", user_info.contact.facebook);
                    } else {
                        console.log("Could not get User Info")
                    }
                }, "GET", "user/info/", "");
                break;
            case 401:
                break;
            default:
                break;
            }
        }
    }
    xhr.send();
}
