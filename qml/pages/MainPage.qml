/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../components"

Page {
    id: page

    Timer {
        running: page.status == PageStatus.Active && settings.readData('AccessToken', "") === ""
        interval: 100
        onTriggered: pageStack.push(Qt.resolvedUrl("Authentication.qml"))
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("About This App")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutThisApp.qml"))
            }
            MenuItem {
                text: qsTr("Friend Requests")
                onClicked: pageStack.push(Qt.resolvedUrl("PendingFriends.qml"))
            }
            MenuItem {
                text: qsTr("Account Info")
                onClicked: pageStack.push(Qt.resolvedUrl("Profile.qml"))
            }
        }

        SearchBeerModel { id: searchBeer }

        contentHeight: column.height

        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                id: header
                title: "Qayttamaton"
            }

            SectionHeader {
                text: qsTr("Search Beer")
            }

            Row {
                id: searchBox
                TextField {
                    x: Theme.paddingLarge
                    width: column.width - clearButton.width
                    id: searchWords
                    placeholderText: qsTr("Name of Beer")
                    font.pixelSize: Theme.fontSizeLarge
                    EnterKey.enabled: text.trim().length > 0
                    EnterKey.onClicked: {
                        pageStack.push(Qt.resolvedUrl("SearchBeerResult.qml"), { "searchWords": text })
                    }
                    clip: true
                }
                IconButton {
                    id: clearButton
                    icon.source: "image://theme/icon-m-clear"
                    onClicked: {
                        searchWords.text = ""
                        searchBeer.clear()
                    }
                }
            }
            Button {
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingLarge
                text: qsTr("Search")
                onClicked: pageStack.push(Qt.resolvedUrl("SearchBeerResult.qml"), { "searchWords": searchWords.text })
                enabled: searchWords.text.trim().length > 0
            }

            SectionHeader {
                text: qsTr("TimeLines")
            }

            PageLink {
                width: parent.width
                linkLabel: qsTr("Recent Activity")
                onClicked: pageStack.push(Qt.resolvedUrl("Activity.qml"))
            }

            PageLink {
                width: parent.width
                linkLabel: qsTr("Near By Activity")
                onClicked: pageStack.push(Qt.resolvedUrl("ThepubLocal.qml"))
            }

            PageLink {
                width: parent.width
                linkLabel: qsTr("Beer Trending")
                onClicked: pageStack.push(Qt.resolvedUrl("BeerTrending.qml"))
            }

            SectionHeader {
                text: qsTr("Notifications")
            }

            PageLink {
                width: parent.width
                linkLabel: qsTr("Notifications for you")
                onClicked: pageStack.push(Qt.resolvedUrl("NotificationsList.qml"))
            }

            PageLink {
                width: parent.width
                linkLabel: qsTr("News Feeds")
                onClicked: pageStack.push(Qt.resolvedUrl("NewsList.qml"))
            }
        }
    }
}
