/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"
import "../models"

Page {
    id: page

    BeerTrendingModel {
        id: beerTrending
    }

    SilicaListView {
        anchors.fill: parent

        model: beerTrending

        header: PageHeader {
            title: qsTr("Beer Trending")
        }

        ViewPlaceholder {
            id: noData
            enabled: false
            text: qsTr("Wish List is not existed")
        }

        delegate: BackgroundItem {
            width: parent.width
            height: beers.height
            Column {
                id: beers
                width: parent.width
                spacing: Theme.paddingSmall
                Separator {
                    width: parent.width
                    color: "#00000000"
                }
                Row {
                    width: parent.width
                    spacing: Theme.paddingSmall
                    Image {
                        id: beerLabel
                        width: beer_column.height
                        height: width
                        source: beer.beer_label
                        BusyIndicator {
                            anchors.centerIn: parent
                            size: BusyIndicatorSize.Medium
                            running: beerLabel.status === Image.Loading
                        }
                    }
                    Column {
                        id: beer_column
                        Text {
                            color: Theme.secondaryHighlightColor
                            font.pixelSize: Theme.fontSizeMedium
                            text: beer.beer_name
                        }
                        Text {
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                            text: brewery.brewery_name
                        }
                        Text {
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                            text: beer.beer_style
                        }
                    }
                }
                Row {
                    width: parent.width
                    spacing: Theme.paddingSmall
                    Rectangle {
                        width: beerLabel.width
                        height: additional_info.height
                        opacity: 0
                    }
                    Column {
                        id: additional_info
                        Text {
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                            text: qsTr("Total Count: %1").arg(total_count)
                        }
                        Text {
                            color: Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeSmall
                            text: qsTr("Your Count: %1").arg(your_count)
                        }
                    }
                }

                Separator {
                    width: parent.width
                    color: Theme.primaryColor
                    horizontalAlignment: Qt.AlignHCenter
                }
            }
            onClicked: pageStack.push(Qt.resolvedUrl("BeerDetail.qml"), { beer_id: beer.bid })
        }

        BusyIndicator {
            anchors.centerIn: parent
            size: BusyIndicatorSize.Large
            running: beerTrending.loading
        }

    }

    Component.onCompleted: beerTrending.load();
}
