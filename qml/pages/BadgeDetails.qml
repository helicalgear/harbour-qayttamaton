/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../components"

Page {
    id: page

    property var badge

    ListModel {
        id: levels
    }

    SilicaGridView {
        id: levelsView
        anchors.fill: parent
        cellWidth: width / 4
        cellHeight: width / 3

        header: Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: badge.badge_name
            }

            Avatar {
                anchors.horizontalCenter: parent.horizontalCenter
                size: parent.width * 0.8
                avatarSource: badge.media.badge_image_lg
            }

            Text {
                width: parent.width * 0.9
                anchors.horizontalCenter: parent.horizontalCenter
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeSmall
                horizontalAlignment: Text.AlignRight
                text: qsTr("Created at: %1").arg(Qt.formatDateTime(new Date(badge.created_at), qsTr("MM/dd/yyyy")))
            }

            Text {
                width: parent.width * 0.9
                anchors.horizontalCenter: parent.horizontalCenter
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.WordWrap
                text: badge.badge_description
            }

            Separator {
                anchors {
                    topMargin: Theme.paddingSmall
                }
                width: parent.width
                color: Theme.primaryColor
                horizontalAlignment: Qt.AlignHCenter
            }

        }

        model: levels

        delegate: BackgroundItem {
            width: badgeInfo.width
            height: badgeInfo.height
            Column {
                id: badgeInfo
                width: levelsView.cellWidth
                height: badgeImage.height + gotDate.contentHeight + badgeName.contentHeight
                anchors.fill: parent
                Avatar {
                    id: badgeImage
                    size: parent.width * 0.8
                    anchors.horizontalCenter: parent.horizontalCenter
                    avatarSource: media.badge_image_md
                }
                Text {
                    id: badgeName
                    width: parent.width
                    color: Theme.primaryColor
                    font.pixelSize: Theme.fontSizeTiny
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    text: badge_name.match(/(Level \d+)/) ? badge_name.match(/(Level \d+)/)[1] : "Level 1"
                }
                Text {
                    id: gotDate
                    width: parent.width
                    color: Theme.primaryColor
                    font.pixelSize: Theme.fontSizeTiny
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    text: Qt.formatDateTime(new Date(created_at), qsTr("MM/dd/yyyy"))
                }
            }
        }
    }

    Component.onCompleted: {
        if ( badge.levels.count > 1 ) {
            for ( var n=0; n < badge.levels.count; n++ ) {
                levels.append(badge.levels.items[n]);
            }
        }
    }
}
