/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SilicaFlickable {
        anchors.fill: parent

        contentHeight: column.height

        Column {
            id: column
            width: page.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("About Qayttamaton")
            }

            SectionHeader {
                text: qsTr("About Qyttamaton")
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Qyttamaton 0.1.3"
            }

            Text {
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.primaryColor
                text: qsTr("Unofficial UNTAPPD client for SailfishOS")
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.primaryColor
                text: "by helicalgear"
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeMedium
                linkColor: Theme.highlightColor
                text: "<a href=\"mailto://helicalgear@gmail.com\">%1</a>".arg(qsTr("Contact developer"))
                onLinkActivated: Qt.openUrlExternally("mailto:helicalgear@gmail.com")
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeMedium
                linkColor: Theme.highlightColor
                text: "<a href=\"https://gitlab.com/helicalgear/harbour-qayttamaton\">%1</a>".arg(qsTr("Access to source code"))
                onLinkActivated: Qt.openUrlExternally(link)
            }

            SectionHeader {
                text: qsTr("About UNTAPPD")
            }

            Image {
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width * 0.5
                fillMode: Image.PreserveAspectFit
                source: "../images/pbu_320_yellow.png"
                MouseArea {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally("https://untappd.com")
                }
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeMedium
                linkColor: Theme.highlightColor
                text: "<a href=\"https://untappd.com/terms\">%1</a>".arg(qsTr("Terms of Service"))
                onLinkActivated: Qt.openUrlExternally(link)
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeMedium
                linkColor: Theme.highlightColor
                text: "<a href=\"https://untappd.com/privacy\">%1</a>".arg(qsTr("Privacy Policy"))
                onLinkActivated: Qt.openUrlExternally(link)
            }

            SectionHeader {
                text: qsTr("Contributers")
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeMedium
                linkColor: Theme.highlightColor
                text: "<a href=\"https://gitlab.com/kempe\">Fredrik Kempe</a>"
                onLinkActivated: Qt.openUrlExternally(link)
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeMedium
                linkColor: Theme.highlightColor
                text: "<a href=\"https://gitlab.com/a-dekker\">Arno Dekker</a>"
                onLinkActivated: Qt.openUrlExternally(link)
            }

            SectionHeader {
                text: qsTr("Special Thanks")
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeMedium
                linkColor: Theme.highlightColor
                text: "<a href=\"http://qt-users.jp/\">%1</a>".arg(qsTr("Japan Qt Users Group"))
                onLinkActivated: Qt.openUrlExternally(link)
            }

        }
    }
}
