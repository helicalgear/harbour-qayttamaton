/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import QtPositioning 5.2
import "../models"

Page {
    id: page

    property var venue
    property bool positionValid: false

    Timer {
        id: startSearch
        interval: 500
        onTriggered: {
            if (positionValid) {
                venuesSearch.load();
                startSearch.stop();
            }
        }
    }

    PositionSource {
        id: positionSource
        active: true
        onPositionChanged: {
            if ( positionSource.position.latitudeValid && positionSource.position.longitudeValid ) {
                var radius = Math.ceil(positionSource.position.horizontalAccuracy / 1000);
                if ( radius <= 0 || radius > 25 ) { radius = 25; }
                venuesSearch.radius = radius * 1000;
                venuesSearch.lat = positionSource.position.coordinate.latitude;
                venuesSearch.lng = positionSource.position.coordinate.longitude;
                positionValid = true;
            }
        }
    }

    SilicaFlickable {
        anchors.fill: parent

        VenuesSearchModel { id: venuesSearch }

        contentHeight: column.height

        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                id: header
                title: "Search Venues"
            }
            Row {
                id: searchBox
                TextField {
                    x: Theme.paddingLarge
                    width: column.width - clearButton.width
                    id: venuesQuery
                    placeholderText: qsTr("Name of Venues")
                    font.pixelSize: Theme.fontSizeLarge
                    EnterKey.enabled: text.trim().length > 0
                    EnterKey.onClicked: {
                        venuesSearch.query = text;
                        startSearch.start();
                    }
                    clip: true
                }
                IconButton {
                    id: clearButton
                    icon.source: "image://theme/icon-m-clear"
                    onClicked: {
                        venuesQuery.text = "";
                        venuesSearch.query = "";
                        startSearch.start();
                    }
                }
            }
            Button {
                id: searchButton
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingLarge
                text: qsTr("Search")
                onClicked: {
                    venuesSearch.query = venuesQuery.text;
                    startSearch.start();
                }
                enabled: venuesQuery.text.trim().length > 0
            }

            SilicaListView {
                width: parent.width - Theme.paddingMedium
                height: page.height - header.height - searchBox.height - searchButton.height - Theme.paddingLarge * 3
                x: Theme.paddingMedium
                spacing: Theme.paddingSmall
                clip: true

                model: venuesSearch

                delegate: BackgroundItem {
                    width: parent.width
                    height: searchResult.height
                    Row {
                        id: searchResult
                        width: parent.width
                        spacing: Theme.paddingSmall
                        Image {
                            id: categoryIcon
                            width: parent.width / 4
                            height: width
                            source: aggregatedIcon
                            BusyIndicator {
                                anchors.centerIn: parent
                                size: BusyIndicatorSize.Medium
                                running: categoryIcon.status === Image.Loading
                            }
                        }
                        Column {
                            Text {
                                color: Theme.secondaryHighlightColor
                                font.pixelSize: Theme.fontSizeMedium
                                text: name || ""
                            }
                            Text {
                                color: Theme.secondaryColor
                                font.pixelSize: Theme.fontSizeSmall
                                text: location.address || ""
                            }
                            Text {
                                color: Theme.secondaryColor
                                font.pixelSize: Theme.fontSizeSmall
                                text: location.city || ""
                            }
                        }
                    }
                    onClicked: {
                        venue = {
                            "id": id,
                            "name": name,
                            "location":{"lat": location.lat,
                                         "lng": location.lng}
                        }
                        pageStack.pop()
                    }
                }

                BusyIndicator {
                    anchors.centerIn: parent
                    size: BusyIndicatorSize.Large
                    running: venuesSearch.loading || !positionValid
                }

            }
        }
    }

    Component.onCompleted: startSearch.start();

    Connections {
        target: Qt.application
        onActiveChanged: Qt.application.active ? positionSource.active = true : positionSource.active = false
    }

}
