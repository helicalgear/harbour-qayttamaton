/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../components"
import "../delegates"
import "../utils/UntappdAPI.js" as Untappd

Page {
    id: page

    property string user_name: ""

    UserInfo { id: userInfo }

    UserCheckinsModel {
        id: userCheckinsModel
        user_name: userInfo.user_name
    }

    UserWishListModel {
        id: userWishListModel
        user_name: userInfo.user_name
    }

    SilicaListView {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                visible: userInfo.relationship === "self"
                text: qsTr("Logout")
                onClicked: {
                    settings.removeData("AccessToken")
                    settings.removeData("facebook")
                    settings.removeData("twitter")
                    settings.removeData("foursquare")
                    pageStack.pop()
                }
            }
            MenuItem {
                visible: userInfo.relationship === "friends"
                text: qsTr("Remove from Firends")
                onClicked: {
                    var request = pageStack.push(Qt.resolvedUrl("FriendRequest.qml"), {request: "remove", user_id: userInfo.uid})
                    request.targetIdChanged.connect(function() {
                        Untappd.getFriendRemove(function (result) {
                            if ( result ) {
                                infoBar.show("Succeeded to send the request.");
                                userInfo.load(user_name);
                            } else {
                                infoBar.show("Failed to send the request.");
                            }
                        }, request.targetId)
                    })
                }
            }
            MenuItem {
                visible: userInfo.relationship === "none"
                text: qsTr("Add to Friends")
                onClicked: {
                    var request = pageStack.push(Qt.resolvedUrl("FriendRequest.qml"), {request: "add", user_id: userInfo.uid})
                    request.targetIdChanged.connect(function() {
                        Untappd.getFriendRequest(function (result) {
                            if ( result ) {
                                infoBar.show("Succeeded to send the request.");
                                userInfo.load(user_name);
                            } else {
                                infoBar.show("Failed to send the request.");
                            }
                        }, request.targetId)
                    })
                }
            }
            MenuItem {
                visible: userInfo.relationship === "pending_you"
                text: qsTr("Reject the Friend Request")
                onClicked: {
                    var request = pageStack.push(Qt.resolvedUrl("FriendRequest.qml"), {request: "reject", user_id: userInfo.uid})
                    request.targetIdChanged.connect(function() {
                        Untappd.getFriendReject(function (result) {
                            if ( result ) {
                                infoBar.show("Succeeded to send the request.");
                                userInfo.load(user_name);
                            } else {
                                infoBar.show("Failed to send the request.");
                            }
                        }, request.targetId)
                    })
                }
            }
            MenuItem {
                visible: userInfo.relationship === "pending_you"
                text: qsTr("Accept the Friend Request")
                onClicked: {
                    var request = pageStack.push(Qt.resolvedUrl("FriendRequest.qml"), {request: "accept", user_id: userInfo.uid})
                    request.targetIdChanged.connect(function() {
                        Untappd.getFriendAccept(function (result) {
                            if ( result ) {
                                infoBar.show("Succeeded to send the request.");
                                userInfo.load(user_name);
                            } else {
                                infoBar.show("Failed to send the request.");
                            }
                        }, request.targetId)
                    })
                }
            }
            MenuItem {
                visible: userInfo.relationship === "pending_them"
                text: qsTr("Not yet approved your request...")
            }
        }

        header: Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: userInfo.user_name
            }

            Image {
                width: parent.width - ( Theme.paddingLarge * 2 )
                height: width / 4
                x: Theme.paddingLarge
                fillMode: Image.PreserveAspectCrop
                source: userInfo.user_cover_photo

                Avatar {
                    anchors.centerIn: parent
                    size: 128
                    avatarSource: userInfo.user_avatar
                }
            }

            Row {
                width: parent.width - ( Theme.paddingLarge * 2 )
                height: parent.width * 0.2
                x: Theme.paddingLarge

                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 20

                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            font.pixelSize: Theme.fontSizeMedium
                            color: Theme.secondaryColor
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("TOTAL")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            font.pixelSize: Theme.fontSizeMedium
                            color: Theme.secondaryHighlightColor
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: userInfo.stats.total_checkins
                        }
                    }
                }

                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 20

                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            font.pixelSize: Theme.fontSizeMedium
                            color: Theme.secondaryColor
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("UNIQUE")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            font.pixelSize: Theme.fontSizeMedium
                            color: Theme.secondaryHighlightColor
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: userInfo.stats.total_beers
                        }
                    }
                }

                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 20

                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            font.pixelSize: Theme.fontSizeMedium
                            color: Theme.secondaryColor
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("FRIENDS")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            font.pixelSize: Theme.fontSizeMedium
                            color: Theme.secondaryHighlightColor
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: userInfo.stats.total_friends
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: pageStack.push(Qt.resolvedUrl("Friends.qml"), { user_name: userInfo.user_name })
                    }
                }

                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 20

                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            font.pixelSize: Theme.fontSizeMedium
                            color: Theme.secondaryColor
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("BADGES")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            font.pixelSize: Theme.fontSizeMedium
                            color: Theme.secondaryHighlightColor
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: userInfo.stats.total_badges
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: pageStack.push(Qt.resolvedUrl("Badges.qml"), { user_name: userInfo.user_name })
                    }
                }
            }

            Separator {
                anchors {
                    topMargin: Theme.paddingSmall
                }
                width: parent.width
                color: Theme.primaryColor
                horizontalAlignment: Qt.AlignHCenter
            }

            PageLink {
                width: parent.width
                linkLabel: qsTr("%1's Wish List\nUpdated on %2").arg(userInfo.user_name).arg(Qt.formatDateTime(new Date(userWishListModel.updated_at)))
                onClicked: pageStack.push(Qt.resolvedUrl("UserWishList.qml"), { user_name: userInfo.user_name })
            }

        }

        model: userCheckinsModel

        delegate: CheckinDelegate {
            onClicked: {
                var request = pageStack.push(Qt.resolvedUrl("CheckinDetails.qml"), { "checkin_id": checkin_id })
                request.onComments_reload.connect(function() {
                    comments_reload();
                })
                request.onToast_reload.connect(function() {
                    toasts_reload();
                })
            }
        }
    }

    BusyIndicator {
        anchors.centerIn: parent
        size: BusyIndicatorSize.Large
        running: userInfo.loading
    }

    Component.onCompleted: userInfo.load(user_name)
}
