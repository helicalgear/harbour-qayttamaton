/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.qayttamaton.OAuth 1.0
import "../utils/UntappdAPI.js" as Untappd

Page {
    id: page

    SilicaFlickable {
        id: authPage
        width: Screen.width
        height: Screen.height

        PageHeader {
            title: qsTr("Authentication")
        }

        Column {
            id: column
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width
            spacing: Theme.paddingLarge

            Text{
                id: message
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeLarge
                text: qsTr("Please Login to UNTAPPD\nwith external browser...")
            }

            Text {
                width: parent.width
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeMedium
                horizontalAlignment: Text.Center
                wrapMode: Text.WordWrap
                text: qsTr("Please tap below button if the external browser is not opened automatically.")
            }

            Button {
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Tap here to login")
                onClicked: Qt.openUrlExternally("https://untappd.com/oauth/authenticate/?client_id=%1&response_type=code&redirect_url=%2".arg(clientId).arg(redirectUrl))
            }


        }

        OAuth {
            id: oauth
            oauthClientId: clientId
            oauthRedirectUrl: redirectUrl

            onCodeChanged: {
                var xhr = new XMLHttpRequest;
                var url = "https://untappd.com/oauth/authorize/?client_id=%1&client_secret=%2&response_type=code&redirect_url=%3&code=%4".arg(clientId).arg(clientSecret).arg(redirectUrl).arg(code);
                xhr.open("GET", url, true);
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === 4) {
                        switch (xhr.status) {
                        case 200:
                            var jsn = JSON.parse(xhr.responseText);
                            settings.saveData('AccessToken', jsn.response.access_token);
                            infoBar.show("Authenticated");
                            pageStack.replace(Qt.resolvedUrl("Profile.qml"));
                            break;
                        default:
                            break;
                        }
                    }
                }
                xhr.send();
            }
        }
        Component.onCompleted: oauth.getCode()
    }
}
