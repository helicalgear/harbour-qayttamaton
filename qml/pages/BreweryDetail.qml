/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../components"
import "../utils/UntappdAPI.js" as Untappd

Page {
    id: page

    property string brewery_id

    BreweryInfo {
        id: breweryInfo
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("Activity of this brewery")
                onClicked: pageStack.push(Qt.resolvedUrl("BreweryActivity.qml"), { "brewery_id": breweryInfo.brewery_id, "brewery_name": breweryInfo.brewery_name })
            }
            MenuItem {
                text: qsTr("Beers of this brewery")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("BeersOfBrewery.qml"), {"brewery_name": breweryInfo.brewery_name, "beer_list": breweryInfo.beer_list } )
                }
            }
        }

        contentHeight: column.height

        Column {
            id: column

            width: page.width - ( Theme.paddingLarge * 2 )
            x: Theme.paddingLarge
            spacing: Theme.paddingMedium
            PageHeader {
                id: header
                title: breweryInfo.brewery_name
            }
            Row {
                width: parent.width
                spacing: Theme.paddingLarge
                Image {
                    width: parent.width / 4
                    height: width
                    fillMode: Image.PreserveAspectFit
                    source: breweryInfo.brewery_label
                }
                Column {
                    width: parent.width * 3 / 4
                    clip: true
                    ScrollingLabel {
                        label_color: Theme.secondaryHighlightColor
                        label_pixelSize: Theme.fontSizeMedium
                        label_text: breweryInfo.brewery_name
                    }
                    Text {
                        color: Theme.secondaryColor
                        font.pixelSize: Theme.fontSizeSmall
                        text: breweryInfo.brewery_type
                    }
                    Text {
                        color: Theme.secondaryColor
                        font.pixelSize: Theme.fontSizeSmall
                        text: breweryInfo.country_name
                    }
                }
            }

            Row {
                FavStar { rate: breweryInfo.rating.rating_score >= 1.0 ? 1.0 : breweryInfo.rating.rating_score }
                FavStar { rate: breweryInfo.rating.rating_score >= 2.0 ? 1.0 : breweryInfo.rating.rating_score < 1.0 ? 0 : breweryInfo.rating.rating_score - 1.0 }
                FavStar { rate: breweryInfo.rating.rating_score >= 3.0 ? 1.0 : breweryInfo.rating.rating_score < 2.0 ? 0 : breweryInfo.rating.rating_score - 2.0 }
                FavStar { rate: breweryInfo.rating.rating_score >= 4.0 ? 1.0 : breweryInfo.rating.rating_score < 3.0 ? 0 : breweryInfo.rating.rating_score - 3.0 }
                FavStar { rate: breweryInfo.rating.rating_score === 5.0 ? 1.0 : breweryInfo.rating.rating_score < 4.0 ? 0 : breweryInfo.rating.rating_score - 4.0 }
                Text {
                    height: parent.height
                    font.pixelSize: Theme.fontSizeMedium
                    color: Theme.secondaryColor
                    verticalAlignment: Text.AlignVCenter
                    text: breweryInfo.rating.rating_score
                }
            }

            Row {
                width: parent.width
                height: width * 0.18
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                            text: qsTr("TOTAL")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryHighlightColor
                            text: breweryInfo.stats.total_count
                        }
                    }
                }
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                            text: qsTr("UNIQUE")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryHighlightColor
                            text: breweryInfo.stats.unique_count
                        }
                    }
                }
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                            text: qsTr("MONTHLY")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryHighlightColor
                            text: breweryInfo.stats.monthly_count
                        }
                    }
                }
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                            text: qsTr("YOU")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryHighlightColor
                            text: breweryInfo.stats.user_count
                        }
                    }
                }
            }

            Text {
                width: parent.width
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeMedium
                text: qsTr("Contact:")
            }
            Row {
                width: parent.width
                height: width * 0.18
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Image {
                        width: parent.height * 0.7
                        height: width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "../images/Twitter.png"
                        opacity: breweryInfo.contact.twitter !== "" ? 1.0 : 0.2
                    }
                    MouseArea {
                        anchors.fill: parent
                        enabled: breweryInfo.contact.twitter !== "" ? true : false
                        onClicked: Qt.openUrlExternally("https://twitter.com/%1".arg(breweryInfo.contact.twitter))
                    }
                }
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Image {
                        width: parent.height * 0.7
                        height: width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "../images/Facebook.png"
                        opacity: breweryInfo.contact.facebook !== "" ? 1.0 : 0.2
                    }
                    MouseArea {
                        anchors.fill: parent
                        enabled: breweryInfo.contact.facebook !== "" ? true : false
                        onClicked: Qt.openUrlExternally(breweryInfo.contact.facebook)
                    }
                }
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Image {
                        width: parent.height * 0.7
                        height: width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "../images/Instagram.png"
                        opacity: breweryInfo.contact.instagram !== "" ? 1.0 : 0.2
                    }
                    MouseArea {
                        anchors.fill: parent
                        enabled: breweryInfo.contact.instagram !== "" ? true : false
                        onClicked: Qt.openUrlExternally("https://www.instagram.com/%1".arg(breweryInfo.contact.instagram))
                    }
                }
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Image {
                        width: parent.height * 0.8
                        height: width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "image://theme/icon-m-website"
                        opacity: breweryInfo.contact.url !== "" ? 1.0 : 0.2
                    }
                    MouseArea {
                        anchors.fill: parent
                        enabled: breweryInfo.contact.url !== "" ? true : false
                        onClicked: Qt.openUrlExternally(breweryInfo.contact.url)
                    }
                }
            }

            Text {
                width: parent.width
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeMedium
                text: qsTr("Description:")
            }
            Text {
                id: description
                width: parent.width
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.WordWrap
                text: breweryInfo.brewery_description
            }


        }
    }

    BusyIndicator {
        anchors.centerIn: parent
        size: BusyIndicatorSize.Large
        running: breweryInfo.loading
    }

    Component.onCompleted: breweryInfo.load(brewery_id)
}
