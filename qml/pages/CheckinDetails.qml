/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../components"

Page {
    id: page

    property string checkin_id

    signal comments_reload()
    signal toast_reload()

    CheckinView {
        id: checkinView
    }

    SilicaFlickable {
        anchors.fill: parent

        contentHeight: column.height + Theme.paddingLarge

        Column {
            id: column
            width: parent.width - Theme.paddingLarge * 2
            x: Theme.paddingLarge
            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("Checkin Details")
            }

            Image {
                id: photo
                width: parent.width
                fillMode: Image.PreserveAspectFit
                source: checkinView.media !== null && checkinView.media.count > 0  ? checkinView.media.items[0].photo.photo_img_og : ""
            }

            UserCheckedIn {
                width: parent.width

                user_avatar: checkinView.user !== null ? checkinView.user.user_avatar : ""
                user_name: checkinView.user !== null ? checkinView.user.user_name : ""
                first_name: checkinView.user !== null ? checkinView.user.first_name : ""
                last_name: checkinView.user !== null ? checkinView.user.last_name : ""
                venue_name: checkinView.venue !== null && checkinView.venue.venue_name ? checkinView.venue.venue_name : ""
                created_at: checkinView.created_at

                onClicked: pageStack.push(Qt.resolvedUrl("Profile.qml"), { "user_name": checkinView.user.user_name })
            }

            BeerCheckedIn {
                width: parent.width

                beer_label: checkinView.beer !== null ? checkinView.beer.beer_label : ""
                beer_name: checkinView.beer !== null ? checkinView.beer.beer_name : ""
                brewery_name: checkinView.brewery !== null ? checkinView.brewery.brewery_name : ""
                beer_style: checkinView.beer !== null ? checkinView.beer.beer_style : ""
                checkin_comment: checkinView.checkin_comment
                rating_score: checkinView.rating_score
                toast: checkinView.toasts !== null ? checkinView.toasts.auth_toast : false
                relationship: checkinView.user !== null ? checkinView.user.relationship : "none"
                badges: checkinView.badges !== null ? checkinView.badges : {}

                onComments_changed: comments_reload()
                onToast_changed: toast_reload()

                onClicked: pageStack.push(Qt.resolvedUrl("BeerDetail.qml"), { "beer_id": checkinView.beer.bid })
            }

            Toasts {
                id: toasts_view
                width: parent.width - Theme.paddingLarge * 2
                x: Theme.paddingLarge

                toasts: checkinView.toasts !== null ? checkinView.toasts : { "count": 0 }

                enabled: true
            }

            Comments {
                id: comments_view
                width: parent.width - Theme.paddingLarge * 2
                x: Theme.paddingLarge

                comments: checkinView.comments !== null ? checkinView.comments : { "count": 0 }

                onComments_changed: comments_reload()

                enabled: true
            }

            Column {
                width: parent.width

                SectionHeader {
                    text: qsTr("Brewery")
                }
                NameLabel {
                    width: parent.width
                    enabled: true

                    label_image: checkinView.brewery !== null ? checkinView.brewery.brewery_label : ""
                    label_name: checkinView.brewery !== null ? checkinView.brewery.brewery_name : ""
                    label_info_1: checkinView.brewery !== null ? checkinView.brewery.brewery_type : ""
                    label_info_2: checkinView.brewery !== null ? checkinView.brewery.country_name : ""

                    onClicked: pageStack.push(Qt.resolvedUrl("BreweryDetail.qml"), { "brewery_id": checkinView.brewery.brewery_id })
                }
            }

            Column {
                width:parent.width
                visible: Object.keys(checkinView.venue).length !== 0

                SectionHeader {
                    text: qsTr("Location")
                }
                NameLabel {
                    width: parent.width
                    visible: Object.keys(checkinView.venue).length !== 0
                    enabled: true

                    label_image: Object.keys(checkinView.venue).length !== 0 ? checkinView.venue.venue_icon.lg : ""
                    label_name: Object.keys(checkinView.venue).length !== 0 ? checkinView.venue.venue_name : ""
                    label_info_1: Object.keys(checkinView.venue).length !== 0 ? "%1 %2, %3".arg(checkinView.venue.location.venue_address).arg(checkinView.venue.location.venue_city).arg(checkinView.venue.location.venue_state) : ""
                    label_info_2: Object.keys(checkinView.venue).length !== 0 && checkinView.venue.categories.count > 0 ? checkinView.venue.categories.items[0].category_name : ""

                    onClicked: pageStack.push(Qt.resolvedUrl("VenueDetail.qml"), { "venue_id": checkinView.venue.venue_id })
                }
            }
        }

        BusyIndicator {
            anchors.centerIn: parent
            size: BusyIndicatorSize.Large
            running: checkinView.loading
        }
    }

    Component.onCompleted: checkinView.load(checkin_id)
}
