/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import "../utils/UntappdAPI.js" as Untappd

ListModel {
    //
    property bool loading: false
    property string endpoint: ""
    property string parameters: ""
    //
    //property int max_id: 0
    //property int min_id: 0
    property int limit: 25
    property string next_url: ""

    function load() {
        loading = true;
        clear();
        Untappd.accessAPI( function(result, response, notifications) {
            if ( result ) {
                var items = response.checkins.items;
                for (var n = 0; n < response.checkins.count; n++) {
                    if (Object.keys(items[n].venue).length === 0) { items[n].venue = {}; }
                    append(items[n]);
                }
                //max_id = response.pagination.max_id;
                //min_id = response.pagination.since_url.match(/min_id=([0-9]+)/)[1];
                if ( response.pagination.next_url !== "" ) {
                    next_url = response.pagination.next_url.match(/\?(.+?)(&|$)/)[1];
                } else {
                    next_url = "";
                }
            } else {
                console.log ("%1: %2".arg(response.code).arg(response.error_detail));
            }
            loading = false;
        }, "GET", endpoint, parameters);
    }

    function loadNext() {
        if ( next_url !== "" ) {
            loading = true;
            Untappd.accessAPI( function(result, response, notifications) {
                if ( result ) {
                    var items = response.checkins.items;
                    for (var n = 0; n < response.checkins.count; n++) {
                        if (Object.keys(items[n].venue).length === 0) { items[n].venue = {}; }
                      append(items[n]);
                    }
                    //max_id = response.pagination.max_id;
                    next_url = response.pagination.next_url.match(/\?(.+?)(&|$)/)[1];
                } else {
                    console.log ("%1: %2".arg(response.code).arg(response.error_detail));
                }
                loading = false;
            }, "GET", endpoint, "%1&%2".arg(parameters).arg(next_url));
        }
    }
/*
    function loadSince() {
        loading = true;
        Untappd.accessAPI( function(result, response, notifications) {
            if ( result ) {
                var items = response.checkins.items;
                for (var n = 0; n < response.checkins.count; n++) {
                    if (Object.keys(items[n].venue).length === 0) { items[n].venue = {}; }
                    insert(n, items[n]);
                }
                min_id = response.pagination.since_url.match(/min_id=([0-9]+)/)[1];
            } else {
                console.log ("%1: %2".arg(response.code).arg(response.error_detail));
            }
            loading = false;
        }, "GET", endpoint, "%1&min_id=%2".arg(parameters).arg(min_id));
    }
*/
}
