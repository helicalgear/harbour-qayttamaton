/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../utils/UntappdAPI.js" as Untappd
import "../components"

BackgroundItem {
    id: root
    width: parent.width - Theme.paddingSmall * 2
    height: checkinsColumn.height + checkinsSeparator.height + Theme.paddingMedium * 4
    x: Theme.paddingSmall

    property string checkin_id: model.checkin_id

    Image {
        id: photo
        anchors.fill: parent
        source: media.count > 0 ? media.items[0].photo.photo_img_md : ""
        fillMode: Image.PreserveAspectCrop
    }

    Rectangle {
        anchors.fill: parent
        color: "#80000000"
        radius: 10
    }

    Column {
        id: checkinsColumn
        width: parent.width
        spacing: Theme.paddingLarge

        UserCheckedIn {
            id: user_info
            width: parent.width
            height: width * 0.2

            user_avatar: user.user_avatar
            user_name: user.user_name
            first_name: user.first_name
            last_name: user.last_name
            venue_name: venue.venue_name ? venue.venue_name : ""
            created_at: model.created_at

            enabled: false
        }

        BeerCheckedIn {
            id: beer_info
            width: parent.width - Theme.paddingLarge * 2
            x: Theme.paddingLarge

            beer_label: beer.beer_label
            beer_name: beer.beer_name
            brewery_name: brewery.brewery_name
            beer_style: beer.beer_style
            checkin_comment: model.checkin_comment
            rating_score: model.rating_score
            toast: toasts.auth_toast
            relationship: user.relationship
            badges: model.badges ? model.badges : {}

            enabled: false
        }

        Toasts {
            id: toasts_view
            width: parent.width - Theme.paddingLarge * 2
            x: Theme.paddingLarge

            toasts: model.toasts

            enabled: false
        }

        Comments {
            id: comments_view
            width: parent.width - Theme.paddingLarge * 2
            x: Theme.paddingLarge

            comments: model.comments

            enabled: false
        }
    }

    Separator {
        id: checkinsSeparator
        anchors {
            bottom: photo.bottom
            topMargin: Theme.paddingSmall
        }
        width: parent.width
        color: Theme.primaryColor
        horizontalAlignment: Qt.AlignHCenter
    }

    function comments_reload() {
        Untappd.getCheckinView(function(checkin_data){
            comments_view.comments = checkin_data["comments"];
        }, checkin_id);
    }

    function toasts_reload() {
        Untappd.getCheckinView(function(checkin_data){
            toasts_view.toasts = checkin_data["toasts"];
        }, checkin_id);
        beer_info.toast = !beer_info.toast
    }

}
