/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../utils/UntappdAPI.js" as Untappd

MouseArea {
    id: root
    height: beer_column.height

    property string beer_label
    property string beer_name
    property string brewery_name
    property string beer_style
    property string checkin_comment
    property real rating_score
    property bool toast
    property string relationship
    property var badges

    signal comments_changed()
    signal toast_changed()

    Column {
        id: beer_column
        width: parent.width
        spacing: Theme.paddingMedium

        Row {
            width: parent.width
            spacing: Theme.paddingSmall

            Image {
                id: label
                width: beer_info.height
                height: width
                source: beer_label
            }

            Column {
                id: beer_info
                width: parent.width - label.width
                clip: true

                Text {
                    width: parent.width
                    color: Theme.secondaryHighlightColor
                    font.pixelSize: Theme.fontSizeMedium
                    text: beer_name
                }

                Text {
                    width: parent.width
                    color: Theme.secondaryColor
                    font.pixelSize: Theme.fontSizeSmall
                    text: brewery_name
                }

                Text {
                    width: parent.width
                    color: Theme.secondaryColor
                    font.pixelSize: Theme.fontSizeSmall
                    text: beer_style
                }
            }
        }

        Text {
            width: parent.width
            visible: checkin_comment !== ""
            font.pixelSize: Theme.fontSizeSmall
            color: Theme.secondaryColor
            wrapMode: Text.WrapAnywhere
            text: checkin_comment
        }

        Row {
            FavStar { rate: rating_score >= 1.0 ? 1.0 : rating_score }
            FavStar { rate: rating_score >= 2.0 ? 1.0 : rating_score < 1.0 ? 0 : rating_score - 1.0 }
            FavStar { rate: rating_score >= 3.0 ? 1.0 : rating_score < 2.0 ? 0 : rating_score - 2.0 }
            FavStar { rate: rating_score >= 4.0 ? 1.0 : rating_score < 3.0 ? 0 : rating_score - 3.0 }
            FavStar { rate: rating_score == 5.0 ? 1.0 : rating_score < 4.0 ? 0 : rating_score - 4.0 }
            Text {
                height: parent.height
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.secondaryColor
                verticalAlignment: Text.AlignVCenter
                text: rating_score
            }
        }

        BadgesCheckedIn {
            width: parent.width - Theme.paddingLarge
            x: Theme.paddingLarge
            enabled: false

            badges: root.badges
        }

        Row {
            visible: relationship !== "none"
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Theme.paddingMedium
            Button {
                text: qsTr("Comment")
                onClicked: {
                    var request = pageStack.push(Qt.resolvedUrl("../../qml/pages/AddComments.qml"), {comments: comments_view.comments})
                    request.commentChanged.connect(function() {
                        Untappd.postCheckinAddcomment(function (result) {
                            if ( result ) {
                                infoBar.show("Succeeded to send the comment.");
                                Untappd.getCheckinView(function(checkin_data){
                                    comments_view.comments = checkin_data["comments"];
                                }, checkin_id);
                                comments_changed();
                            } else {
                                infoBar.show("Failed to send the comment.");
                            }
                        }, checkin_id.toString(), request.comment)
                    })
                }            }
            Button {
                text: down ? qsTr("Toasted") : qsTr("Toast")
                down: toast
                onClicked: Untappd.postCheckinToast(function(result, type) {
                    if ( result === "success" ) {
                        if ( type === "toast" ) {
                            infoBar.show(qsTr("Toasted !"))
                            down = true
                            Untappd.getCheckinView(function(checkin_data){
                                toasts_view.toasts = checkin_data["toasts"];
                            }, checkin_id);
                        } else {
                            infoBar.show(qsTr("Untoasted !"))
                            down = false
                            Untappd.getCheckinView(function(checkin_data){
                                toasts_view.toasts = checkin_data["toasts"];
                            }, checkin_id);
                        }
                        toast_changed();
                    } else {
                        infoBar.show(qsTr("Toast/Untoast Failed..."))
                    }
                }, checkin_id.toString())
            }
        }
    }
}
