/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../utils/UntappdAPI.js" as Untappd

Item {
    id: root
    width: parent.width

    property var comments

    signal comments_changed()

    ListModel {
        id: commentList
    }

    ListView {
        anchors.fill: parent
        visible: comments.count > 0
        boundsBehavior: Flickable.StopAtBounds

        model: commentList

        delegate: BackgroundItem {
            width: parent.width
            height: comment_text.height
            enabled: user.relationship === "self" ? true : false
            Row {
                anchors.fill: parent
                spacing: Theme.paddingSmall
                Avatar {
                    id: avatar
                    size: Theme.fontSizeSmall
                    avatarSource: user.user_avatar
                }
                Column {
                    id: comment_text
                    width: parent.width - avatar.width
                    height: name.contentHeight + text.contentHeight
                    Row {
                        id:header
                        width: parent.width
                        Text {
                            id: name
                            font.pixelSize: Theme.fontSizeExtraSmall
                            color: user.relationship === "self" ? Theme.highlightColor : Theme.primaryColor
                            wrapMode: Text.WrapAnywhere
                            text: "%1 %2".arg(user.first_name).arg(user.last_name)
                        }
                        Text {
                            id: time
                            width: parent.width - name.width
                            font.pixelSize: Theme.fontSizeExtraSmall
                            color: user.relationship === "self" ? Theme.highlightColor : Theme.secondaryColor
                            wrapMode: Text.WrapAnywhere
                            horizontalAlignment: Text.AlignRight
                            text: Qt.formatDateTime(new Date(created_at))
                        }
                    }
                    Text {
                        id: text
                        width: parent.width
                        font.pixelSize: Theme.fontSizeExtraSmall
                        color: user.relationship === "self" ? Theme.highlightColor : Theme.secondaryColor
                        wrapMode: Text.WrapAnywhere
                        text: comment
                    }
                }
            }
            Component.onCompleted: root.height = root.height + comment_text.height

            onClicked: {
                var request = pageStack.push(Qt.resolvedUrl("../../qml/pages/DeleteComments.qml"), {user: user, created_at: created_at, comment: comment, comment_id: comment_id})
                request.delRequestChanged.connect(function() {
                    if ( request.modify ) {
                        // Add new comment first
                        Untappd.postCheckinAddcomment(function (result_add) {
                            if ( result_add ) {
                                // Delete old comment if AddComment is succeeded
                                Untappd.postCheckinDeletecomment(function (result_del) {
                                    if (result_del) {
                                        // result_add =true, result_del = true
                                        infoBar.show("Comment is modified successfully");
                                        Untappd.getCheckinView(function(checkin_data){
                                            comments_view.comments = checkin_data["comments"];
                                            comments_changed();
                                        }, checkin_id.toString());
                                    } else {
                                        // result_add = true, result_del = false
                                        infoBar.show("Failed to clean-up the old comment");
                                        Untappd.getCheckinView(function(checkin_data){
                                            comments_view.comments = checkin_data["comments"];
                                            comments_changed();
                                        }, checkin_id.toString());
                                    }
                                }, comment_id.toString())
                            } else {
                                // result_add = false
                                infoBar.show("Failed to modify the comment.");
                            }
                        }, checkin_id.toString(), request.mod_comment)
                    } else {
                        Untappd.postCheckinDeletecomment(function (result) {
                            if ( result ) {
                                infoBar.show("Succeeded to delete the comment.");
                                Untappd.getCheckinView(function(checkin_data){
                                    comments = checkin_data["comments"];
                                    comments_changed();
                                }, checkin_id.toString());
                            } else {
                                infoBar.show("Failed to delete the comment.");
                            }
                        }, comment_id.toString())
                    }
                })
            }
        }
    }

    onCommentsChanged: if ( comments.count > 0 ) {
                                commentList.clear();
                                root.height = 0;
                                for (var n=0; n < comments.count; n++) {
                                    commentList.append(comments.items[n]);
                                }
                            }
}
