import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    id: root
    width: parent.width
    height: toast_view.height
    visible: toasts.count > 0

    property var toasts

    ListModel {
        id: toastList
    }

    Row {
        id: toast_view
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width

        Image {
            id: toast_icon
            source: "image://theme/icon-s-like"
        }
        Text {
            id: toast_count
            width: Theme.iconSizeMedium
            font.pixelSize: Theme.iconSizeSmall
            color: Theme.secondaryColor
            horizontalAlignment: Text.left
            text: toasts.count
        }

        ListView {
            anchors.left: toast_count.right
            width: parent.width - ( toast_icon.width + toast_count.width )
            orientation: Qt.Horizontal
            x: Theme.paddingSmall

            model: toastList

            delegate: BackgroundItem {
                height: Theme.iconSizeSmall
                width: height * 1.5
                Avatar {
                    anchors.horizontalCenter: parent.horizontalCenter
                    size: parent.height
                    avatarSource: user.user_avatar
                }
                onClicked: pageStack.push(Qt.resolvedUrl("../pages/Profile.qml"), { "user_name": user.user_name })

            }
        }
    }

    Separator {
        anchors.top: parent.bottom
        width: parent.width
        color: Theme.primaryColor
        horizontalAlignment: Qt.AlignHCenter
    }

    onToastsChanged: if ( toasts.count > 0 ) {
                         toastList.clear();
                         for (var n=0; n < toasts.count; n++) {
                             toastList.append(toasts.items[n]);
                         }
                     }

}
